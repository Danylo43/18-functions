"use stric";

//1. Напишіть функцію, яка повертає частку двох чисел.
//Виведіть результат роботи функції в консоль.

function numberSim(a, b) {
if (a === 0 || b === 0)  {
console.log("Ділення на нуль неможливе");
return console.warn(null);
}

  return a / b;
}

const result = numberSim(12, 2);
if (result !== null ) {
  console.log(`Результат ділення: ${result}`);
}

//2. Завдання: Реалізувати функцію, яка виконуватиме математичні
//операції з введеними користувачем числами.

//Технічні вимоги:
//- Отримати за допомогою модального вікна браузера два числа.
//Провалідувати отримані значення(перевірити, що отримано числа).
//Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
//- Отримати за допомогою модального вікна браузера математичну операцію,
// яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати
//отримане значення. Якщо користувач ввів не передбачене значення, вивести
//alert('Такої операції не існує').
//- Створити функцію, в яку передати два значення та операцію.
//- Вивести у консоль результат виконання функції.

//Перевірка чисел
function getNumber(messageNumber) {
  let number;
  while (true) {
    number = prompt(messageNumber);
    if (number !== null && number.trim() !== "") {
      if (!isNaN(number)) {
        return parseFloat(number);
      } else {
        alert("Помилка! Ваше значення НЕ є числом");
      }
    } else {
      alert("Помилка! Ваше значення НЕ є коректим");
    }
  }
}

//Операції
function operation() {
  let operation;
  const massOperations = ["+", "-", "*", "/"];
  while (true) {
    operation = prompt("Введіть необхідну операцію над числами");
    if (massOperations.includes(operation)) {
      return operation;
    } else {
      alert("Помилка! Такої операції не існує");
    }
  }
}

//Операції над числами
function calculate(numberFirst, numberSecond, operation) {
  switch (operation) {
    case "+":
      return numberFirst + numberSecond;
    case "-":
      return numberFirst - numberSecond;
    case "*":
      return numberFirst * numberSecond;
    case "/":
      if (numberFirst !== 0 && numberSecond !== 0) {
        return numberFirst / numberSecond;
      } else {
        return "Помилка! Ділення на нуль заборонено";
      }
    default:
      return "Помилка! Невідома операція";
  }
}

function main() {
  const numberFirst = getNumber("Введіть будь-ласка перше число");
  const numberSecond = getNumber("Введіть будь-ласка друге число");

  const operationInput = operation();

  const result = calculate(numberFirst, numberSecond, operationInput);
  console.log(`Результат: ${result}`);
}

main();


//3. Опціонально. Завдання:

//Реалізувати функцію підрахунку факторіалу числа.
//Технічні вимоги:
//- Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.


// function getNumberFactorial(messageNumber) {
//  let number; 
//  while(true) {
//   number = prompt(messageNumber);
// if(number !== null && number.trim() !== "") {
//   if(!isNaN(number)) {
//     return parseFloat(number);
//   } else {
//     alert("Помилка! Ваше значення НЕ є числом");
//   }
// } else {
//   alert("Помилка! Некорректне значення");
// }
//  }
// }

// function factorial(n) {
//   return (n != 1) ? n * factorial(n - 1) : 1;
// }



// function main() {
// const userInputNum = getNumberFactorial("Введіть будь-ласка число, щоб порахувати його факторіал");
// if(userInputNum !== 0) {
//   const result = factorial(userInputNum);
//   console.log(`Факторіал вашого введеного числа: ${userInputNum} буде за результатом: ${result}`);
// } else {
//   console.log("Помилка!")
// }

// }


// main();